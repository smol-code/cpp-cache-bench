#!/bin/bash

cmake -DCMAKE_BUILD_TYPE=Release -DBENCHMARK_DOWNLOAD_DEPENDENCIES=on -S ./ -B ./build || exit
cmake --build ./build --parallel 16 || exit
cd ./build || exit
echo
echo "----- built successfully -----"
echo
./cache-benching
