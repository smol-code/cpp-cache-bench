#include <benchmark/benchmark.h>
#include <cstdlib>
#include <stdexcept>

// vpetrigo
#include "cache.hpp"
#include "hiredis.h"
#include "lru_cache_policy.hpp"
// nitnelave
#include "../third_party/nitnelave_lru_cache/lru_cache/lru_cache.h"
#include "read.h"



constexpr int FIBO_SIZE = 30;
// doesn't apply to redis
constexpr int FIBO_INPROCESS_CACHE_SIZE = 10;

// In namespace to make static
namespace {
auto cached_fibo(int n, lru_cache::NodeLruCache<int, size_t> &cache) -> size_t
{
	if (n < 2) {
		return 1;
	}
	const size_t *maybe = cache.get_or_null(n);
	if (maybe != nullptr) {
		return *maybe;
	}
	size_t result = cached_fibo(n - 1, cache) + cached_fibo(n - 2, cache);
	cache.insert(n, result);
	return result;
}

auto cached_fibo(int n, caches::fixed_sized_cache<int, size_t, caches::LRUCachePolicy> &cache) -> size_t
{
	if (n < 2) {
		return 1;
	}
	if (cache.Cached(n)) {
		return cache.Get(n);
	}
	size_t result = cached_fibo(n - 1, cache) + cached_fibo(n - 2, cache);
	cache.Put(n, result);
	return result;
}

auto cached_fibo(int n, redisContext *ctx_cache) -> size_t
{
	if (n < 2) {
		return 1;
	}
	redisReply *reply = nullptr;
	reply = static_cast<redisReply *>(redisCommand(ctx_cache, "GET %d", n));
	if (reply != nullptr && reply->str != nullptr) {
		// freeReplyObject(reply);
		return std::stoi(reply->str);
	}

	size_t result = cached_fibo(n - 1, ctx_cache) + cached_fibo(n - 2, ctx_cache);
	redisCommand(ctx_cache, "SET %d %d", n, result);

	return result;
}

auto base_fibo(int n) -> size_t
{
	if (n < 2) {
		return 1;
	}
	size_t result = base_fibo(n - 1) + base_fibo(n - 2);
	return result;
}
}



auto bm_baseline_fibo(benchmark::State &state) -> void
{
	for (auto nop : state) {
		benchmark::DoNotOptimize(base_fibo(FIBO_SIZE));
	}
}

auto bm_nitnelave_fibo(benchmark::State &state) -> void
{
	for (auto nop : state) {
		lru_cache::NodeLruCache cache = lru_cache::make_node_lru_cache<int, size_t>(FIBO_INPROCESS_CACHE_SIZE);
		benchmark::DoNotOptimize(cached_fibo(FIBO_SIZE, cache));
	}
}

auto bm_vpetrigo_fibo(benchmark::State &state) -> void
{
	for (auto nop : state) {
		caches::fixed_sized_cache<int, size_t, caches::LRUCachePolicy> cache(FIBO_INPROCESS_CACHE_SIZE);
		benchmark::DoNotOptimize(cached_fibo(FIBO_SIZE, cache));
	}
}

auto bm_nitnelave_fibo_persistent(benchmark::State &state) -> void
{
	lru_cache::NodeLruCache cache = lru_cache::make_node_lru_cache<int, size_t>(FIBO_INPROCESS_CACHE_SIZE);
	for (auto nop : state) {
		benchmark::DoNotOptimize(cached_fibo(FIBO_SIZE, cache));
	}
}

auto bm_vpetrigo_fibo_persistent(benchmark::State &state) -> void
{
	caches::fixed_sized_cache<int, size_t, caches::LRUCachePolicy> cache(FIBO_INPROCESS_CACHE_SIZE);
	for (auto nop : state) {
		benchmark::DoNotOptimize(cached_fibo(FIBO_SIZE, cache));
	}
}

auto bm_redis_fibo_net(benchmark::State &state) -> void
{
	redisContext *ctx_cache = redisConnect("127.0.0.1", 6379);
	if (ctx_cache == nullptr) {
		throw std::runtime_error("redis client failed to connect (nullptr)");
	}
	if (ctx_cache->err != 0) {
		// redisFree(ctx_cache);
		throw std::runtime_error("redis client failed to connect (err:" + std::to_string(ctx_cache->err) + " )");
	}

	for (auto nop : state) {
		benchmark::DoNotOptimize(cached_fibo(FIBO_SIZE, ctx_cache));
	}

	redisFree(ctx_cache);
}

auto bm_redis_fibo_sock(benchmark::State &state) -> void
{
	redisContext *ctx_cache = redisConnectUnix("/run/redis/redis.sock");
	if (ctx_cache == nullptr) {
		throw std::runtime_error("redis client failed to connect (nullptr)");
	}
	if (ctx_cache->err != 0) {
		// redisFree(ctx_cache);
		throw std::runtime_error("redis client failed to connect (try running as sudo?) (err:" + std::to_string(ctx_cache->err) + " )");
	}

	for (auto nop : state) {
		benchmark::DoNotOptimize(cached_fibo(FIBO_SIZE, ctx_cache));
	}

	redisFree(ctx_cache);
}
BENCHMARK(bm_baseline_fibo);
BENCHMARK(bm_nitnelave_fibo);
BENCHMARK(bm_vpetrigo_fibo);
BENCHMARK(bm_nitnelave_fibo_persistent);
BENCHMARK(bm_vpetrigo_fibo_persistent);
BENCHMARK(bm_redis_fibo_net);
BENCHMARK(bm_redis_fibo_sock);
