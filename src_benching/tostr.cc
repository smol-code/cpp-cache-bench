#include <benchmark/benchmark.h>
#include <random>
#include <sstream>
#include <string>
#include <vector>

// vpetrigo
#include "cache.hpp"
#include "lru_cache_policy.hpp"
// nitnelave
#include "../third_party/nitnelave_lru_cache/lru_cache/lru_cache.h"



// Add [] operator over vpetrigo's impl
// Adapted from nitnelav's adapter as I couldn't make the original variant work.
template <typename Key, typename Value, size_t Size, typename CallbackGet> class vpetrigo_wrapper {
public:
	// vpetrigo_lru_tostring_operator(const std::function<void()>& cb_func) : callback_func(cb_func) {};
	explicit vpetrigo_wrapper(CallbackGet p_cb_func)
		: callback_func(p_cb_func) {};

	auto operator[](const Key &key) -> const Value &
	{
		if (!actual_cache.Cached(key)) {
			Value val = callback_func(key);
			actual_cache.Put(key, val);
		}
		return actual_cache.Get(key);
	}

private:
	caches::fixed_sized_cache<Key, Value, caches::LRUCachePolicy> actual_cache { Size };
	// std::function<void()> callback_func;
	CallbackGet *callback_func;
};



constexpr size_t MISS_RATE = 5;
constexpr double MISS_RATIO = MISS_RATE / 100.0;
constexpr size_t BENCH_SIZE = 1000;
constexpr size_t CACHE_SIZE = 100;
constexpr size_t MAX_VALUE = CACHE_SIZE + (CACHE_SIZE * MISS_RATIO / (1 - MISS_RATIO));

// In a namespace cause of warning:
// - Function 'setup_tostr' declared 'static', move to anonymous namespace instead
namespace {
auto setup_tostr() -> std::vector<int>
{
	std::vector<int> keys(BENCH_SIZE);
	std::mt19937 gen(0); // Using constant seed.
	std::uniform_int_distribution<> dist(0, MAX_VALUE);
	std::generate(keys.begin(), keys.end(), [&]() { return dist(gen); });
	return keys;
}

auto to_string_stream(int val) -> std::string
{
	std::stringstream out;
	out << val;
	return std::move(out).str();
}
}




auto bm_baseline_tostr(benchmark::State &state) -> void
{
	auto keys = setup_tostr();
	for (auto nop : state) {
		volatile size_t total_size = 0;
		for (int key : keys) {
			benchmark::DoNotOptimize(total_size += to_string_stream(key).size());
		}
	}
}

auto bm_nitnelave_tostr(benchmark::State &state) -> void
{
	auto keys = setup_tostr();
	auto cache = lru_cache::make_node_lru_cache<int, std::string>(CACHE_SIZE, to_string_stream);
	for (auto nop : state) {
		size_t total_size = 0;
		for (int key : keys) {
			total_size += cache[key].size();
			benchmark::DoNotOptimize(total_size += cache[key].size());
		}
	}
}

auto bm_vpetrigo_tostr(benchmark::State &state) -> void
{
	auto keys = setup_tostr();
	vpetrigo_wrapper<int, std::string, CACHE_SIZE, decltype(to_string_stream)> cache(to_string_stream);
	for (auto nop : state) {
		size_t total_size = 0;
		for (int key : keys) {
			total_size += cache[key].size();
			benchmark::DoNotOptimize(total_size += cache[key].size());
		}
	}
}

BENCHMARK(bm_baseline_tostr);
BENCHMARK(bm_nitnelave_tostr);
BENCHMARK(bm_vpetrigo_tostr);
