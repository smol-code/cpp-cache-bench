## cpp-lru-cache-bench
Very naive and very simple benchmark of nitnelave's [lru_cache](https://github.com/nitnelave/lru_cache?tab=readme-ov-file) and vpetrigo's [caches](https://github.com/vpetrigo/caches)

This adapts code from lru_cache for the benchmark.

Most libraries are cloned into `./third_party/`. Just clone the git submodules:
- `> git clone --recurse-submodules -j8 git@gitlab.com:smol-code/cpp-cache-bench.git`

Google's [benchmark](https://github.com/google/benchmark) and its [GoogleTest](https://github.com/google/googletest) dependency are fetched with CMake.



### Reasoning
I wanted to compare in-process memory caching to a solution such as redis.

Although the terms are not fair, since redis is not in-process and is networked, for a use case of a simple cache on a single monolithic server, in-process caches are viable.


I assumed the lru_cache benchmark results were at least semi-accurate -- that is, of the single threaded libs, it's probably the fastest. However, I wanted to explicitly test it against vpetrigo's `caches` because the latter is thread-safe and that's a requirement for me.



### TODO
- I want to naively try to wrap nitnelave's library with mutexes, and naively try to use `absl::node_hash_set` in vpetrigo's lib, then compare.

